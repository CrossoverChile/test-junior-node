# Crossover Test Node() {

## Desafio

Se necesita mostrar un sitio web que realice la conversión de UF a CLP, crear un input, un botón y un label de "resultado"(ver imagen) ![image](./image/mockup.png)\
Es necesario realizar la conexión de este "front" con una API Rest programada con Node JS, crear un endpoint, recibir el parámetro desde el "front" y realizar la conversión
necesaria para entregar la respuesta y actualizar el label "resultado" en el "front". Cada request a la API de [mindicador.cl](https://mindicador.cl) tiene una probabilidad de fallar del 10%, para simular este error es necesario realizar la siguiente validación (Javascript):

`if (Math.random(0, 1) < 0.1) throw new Error('Ohhh la peticion ha fallado!')`

Este fallo deberá ser guardado en un archivo `.log` en la raíz del proyecto, para esto pueden realizarlo de la forma que prefieras, como recomendación se puede utilizar [log4js](https://www.npmjs.com/package/log4js). Finalmente, es necesario repetir la llamada a la API cada vez que entregue un error hasta conseguir el dato para la aplicación.

> **Nota**: Para conseguir el valor actual de la UF, recomendamos el uso de la API de [mindicador.cl](https://mindicador.cl)

## Tecnologías a utilizar

- [Node JS](https://nodejs.org/es/)
- [Express](https://expressjs.com/es/)
- [Cualquier framework o libreria front-end (Deseable React)](https://es.reactjs.org/)
